#!/bin/bash

# fix_upstream.sh: make a prboom upstream tarball DFSG-compliant
# Copyright (C) 2007 Jon Dowland <jon@alcopop.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# the GNU General Public Licence can be found in
# /usr/share/common-licences/GPL-2 on Debian systems.


set -u
set -e

# fix_upstream.sh: take a prboom tarball as an argument, strip out
# the non-dfsg free stuff.

# requires deutex to be installed

usage() { echo "usage: $0 tarball" >&2; }

if [ $# -ne 1 ]; then
	usage
	exit 1
fi
if [ ! -r "$1" ]; then
	echo "cannot read '$1'" >&2
	exit 1
fi
ORIG="$1"

# orig should be prboom-x.y.z.tar.gz
ORIGRE='prboom-[0-9]\+\.[0-9]\+\.[0-9]\+\.tar\.gz'
if ! echo "$ORIG" | grep -q "$ORIGRE"; then
	echo "input file should match the regexp '$ORIGRE'" >&2
	exit 1
fi
ORIGVERSION=`echo "$ORIG" | sed 's!prboom-\(.*\)\.tar\.gz!\1!'`
DFSGVERSION="$ORIGVERSION+dfsg3"
ORIGDIR=`pwd`
WD=`mktemp -t -d prboom_fix_upstream.XXXXXX`
trapfun() {
	echo "working directory was \"$WD\""
	cd "$ORIGDIR"
}
trap trapfun INT QUIT

	cp -p "$ORIG" "$WD"
	ORIG=`basename "$ORIG"`
	cd "$WD"

	tar -xf "$ORIG"
	if [ ! -d "prboom-$ORIGVERSION" ]; then
		echo "the tarball did not unpack to "\
		     "\"prboom-$ORIGVERSION\" as expected" >&2
		exit 1
	fi

		# we'll assume the input files are in debian/* unless
		# we're already in a directory called debian
		# WD is "debian" in which case
		BADLUMPS=bad_menu_lumps.txt
		if [ `basename "$ORIGDIR"` != "debian" ]; then
			BADLUMPS="debian/$BADLUMPS"
		fi
		BADLUMPS="$ORIGDIR/$BADLUMPS"

		mkdir wad
		mv "prboom-$ORIGVERSION/data/prboom.wad" wad/prboom.wad
		mv "prboom-$ORIGVERSION/data/prboom.txt" wad/prboom.txt
		cd wad

		deutex -gif -rgb 0 255 255 -xtract prboom.wad >/dev/null
		# these psuedo-lumps are used as markers in prboom but
		# deutex forgets about them on extract
		touch lumps/{c,b}_{start,end}.lmp

		# remove the DOG resources
		<prboom.txt \
			grep -v DOG  |
			grep -v DSDG \
		>wadinfo.txt

		# simply remove the bad menu lumps, they will get substituted
		# by plain text in a small font
		LUMPLIST=`sed "s/\.gif.*$//g" "$BADLUMPS"`
		for l in $LUMPLIST; do
			sed -i "/^$l/Id" wadinfo.txt
		done

		# repack the wad
		mv prboom.wad prboom.wad.old
		deutex -gif -rgb 0 255 255 -build prboom.wad >/dev/null

		# move the fixed wad back over
		cd ..
		mv wad/prboom.wad "prboom-$ORIGVERSION/data/prboom.wad"
		mv wad/prboom.txt "prboom-$ORIGVERSION/data/prboom.txt"
		rm -r wad

	# repack
	mv "prboom-$ORIGVERSION" "prboom-$DFSGVERSION"
	tar -czf "prboom_$DFSGVERSION.orig.tar.gz" "prboom-$DFSGVERSION"
	rm -r "prboom-$DFSGVERSION"

	rm "$ORIG"

cd "$ORIGDIR"

mv "$WD/prboom_$DFSGVERSION.orig.tar.gz" .
echo "created prboom_$DFSGVERSION.orig.tar.gz"
rmdir "$WD"
